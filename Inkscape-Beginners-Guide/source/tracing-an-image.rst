****************
Tracing an Image
****************

|Icon for Trace Bitmap dialog| :kbd:`Shift` + :kbd:`Alt` + :kbd:`B`, :menuselection:`Path --> Trace Bitmap`

You can use this feature to turn a raster image to paths that you can use
and edit in your design. This process is called 'Tracing' or 'Vectorization'.
If you expect a 100% faithful representation of your picture, though, only in
vector format, you will be disappointed.

The functionality is ideal to vectorize dark silhouettes in front of a
bright background. There is an option for keeping colors, but Inkscape
will then create one object for each color. You'll find yourself facing
a pile of objects, each of a different color. This can be difficult to
edit, especially for a beginner.

To vectorize a picture:

.. TODO: give an image example! (e.g. a \*.jpg, \*.png, \*.bmp file)
   
- Import a suitable bitmap image by using the menu
  :menuselection:`File --> Import`.
-  Select the image with the Selector tool.
-  In the menu, go to :menuselection:`Path --> Trace Bitmap`.
-  A dialog will open where you can set different options.
-  Use the :guilabel:`Update` button to get a new preview image whenever you
   change the settings. When the result of the preview looks right, click
   :guilabel:`Ok`. The vectorized image will be available right on the canvas. It
   will be positioned exactly above your picture.

.. Tip::
   Sometimes, the vectorized version will be so
   similar to the original bitmap image, that you do not notice a difference.
   Use the Selector tool to move it to a different place on the canvas, so you
   can examine it properly.

Let's take a closer look at the options for this wonderful tool. The first tab
offers several different choices in the :guilabel:`Mode` tab:

Brightness Cutoff Mode
  This is the most frequently used mode. It will create a silhouette-like path that follows the shape of your image.

Edge Detection Mode
  Useful if you only want to vectorize the contours of a shape.

Color Quantization Mode
  This traces along borders between different colors.

The mode :guilabel:`Multiple Scans` will give you a more detailed result, but it
will create a separate object for each scan.

Don't forget to refresh the preview on the right of the dialog, and to
click on :guilabel:`Ok` to create the vector object.

.. figure:: images/rocket.png
    :alt: A rocket clipart image
    :class: screenshot

    rocket.png, the image that we want to trace in this example.

.. figure:: images/trace_bitmap_dialog_brightness_cutoff_rocket.png
    :alt: The Trace Bitmap dialog with live preview for black-and-white tracing.
    :class: screenshot

    The image rocket.png has been imported and the Trace Bitmap dialog
    was opened. The Live Preview can be activated to show a rough
    preview of the result.

.. figure:: images/trace_bitmap_result_brightness_cutoff.png
    :alt: The rocket traced in brightness cutoff mode
    :class: screenshot

    The rocket traced with the Brightness Cutoff option.

.. figure:: images/trace_bitmap_result_edge_detection.png
    :alt: The rocket traced in edge detection mode
    :class: screenshot

    The rocket vectorized with the Edge Detection option.

.. figure:: images/puzzle_piece.png
    :alt: An image of a puzzle piece
    :class: screenshot

    A bitmap image in color.

.. figure:: images/trace_bitmap_result_multi_colors.png
    :alt: The puzzle piece traced with multiple scans, colors, 10 stacked scans
    :class: screenshot

    Multiple Scans: Colors option with 10 scans. There are 10 stacked
    objects in the result.

.. figure:: images/trace_bitmap_result_multi_colors_shifted.png
    :alt: The stacked scans can be moved after ungrouping.
    :class: screenshot

    After ungrouping, the 10 objects can be moved.

.. |Icon for Trace Bitmap dialog| image:: images/trace_bitmap_icon.png
   :width: 40px
   :height: 40px
